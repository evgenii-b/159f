import { createRouter, createWebHistory } from "vue-router"
import HomeView from "../views/HomeView.vue"

const routes = [
  {
    path: "/",
    name: "home",
    component: HomeView,
  },
  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/AboutView.vue"),
  },
  {
    path: "/artists",
    name: "artists",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/ArtistsView.vue"),
  },
  {
    path: "/artists/:id",
    name: "artist",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/ArtistView.vue"),
  },
  {
    path: "/exhibitions",
    name: "exhibitions",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/ExhibitionsView.vue"),
  },
  {
    path: "/exhibitions/:id",
    name: "exhibition",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/ExhibitionView.vue"),
  },
  {
    path: "/multimedia",
    name: "multimedia",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/ExhibitionsView.vue"),
  },
  {
    path: "/fairs",
    name: "fairs",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/FairsView.vue"),
  },
  {
    path: "/catalogue",
    name: "catalogue",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/CatalogueView.vue"),
  },
  {
    path: "/contacts",
    name: "contacts",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/ContactsView.vue"),
  },
]

const router = createRouter({
  mode: "history",
  history: createWebHistory(process.env.BASE_URL),
  routes,
  // scrollBehavior: (to, from, savedPosition) => {
  //   if (savedPosition) {
  //     return savedPosition
  //   } else if (to.hash) {
  //     return {
  //       selector: to.hash,
  //     }
  //   } else {
  //     return { x: 0, y: 0 }
  //   }
  // },
})

router.beforeEach(function (to, from, next) {
  setTimeout(() => {
    window.scrollTo(0, 0)
  }, 100)
  next()
})

export default router
