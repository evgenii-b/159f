import { createStore } from "vuex"

export default createStore({
  state: {
    params: {
      address: "Moscow, Russia, Kozihensky Pereulok 27",
      instUrl: "https://instagram.com",
    },
    exhibitions: [
      {
        id: 1,
        name: "The Observer's Affect, The 6th Moscow International Biennale for Young Art, CCA Winzavod, curator Arseniy Sergeev",
        name_h1: "The Observer's Affect, The 6th Moscow International Biennale",
        releasePart:
          "The Observer's Affect, The 6th Moscow International Biennale for Young Art, CCA Winzavod, curator Arseniy Sergeev The Observer's Affect, The 6th Moscow International Biennale for Young Art, CCA Winzavod, curator Arseniy Sergeev",
        year: 2021,
        dates: "1-30 Sep",
        place: "Cube Moscow",
        artistId: 1,
        catalogue: "/",
      },
      {
        id: 2,
        name: "The Observer's Affect, The 6th Moscow International Biennale for Young Art, CCA Winzavod, curator Arseniy Sergeev",
        name_h1: "The Observer's Affect, The 6th Moscow International Biennale",
        releasePart:
          "The Observer's Affect, The 6th Moscow International Biennale for Young Art, CCA Winzavod, curator Arseniy Sergeev The Observer's Affect, The 6th Moscow International Biennale for Young Art, CCA Winzavod, curator Arseniy Sergeev",
        year: 2022,
        dates: "12-25 Dec",
        place: "Not Cube Moscow",
        artistId: 1,
        catalogue: "",
      },
    ],
    artists: [
      {
        id: 1,
        name: "Eugeniy Malishev",
        url: "evgeniy-malishev",
        folder: "paints",
        startPhoto: "Start.jpg",
        paints: [
          {
            id: 1,
            name: "Waves",
            params: "2020, Argalit, acril, 100x150cm",
            price: 300000,
            exh: 1,
            or: "h",
            fileName: "paint-0.jpg",
          },
          {
            id: 2,
            name: "Kids",
            params: "2020, Argalit, acril, 100x150cm",
            price: 300000,
            exh: 1,
            or: "v",
            fileName: "paint-1.jpg",
          },
          {
            id: 3,
            name: "Figures 1",
            params: "2020, Argalit, acril, 100x150cm",
            price: 300000,
            exh: 1,
            or: "v",
            fileName: "paint-2.jpg",
          },
          {
            id: 4,
            name: "Figures 2",
            params: "2020, Argalit, acril, 100x150cm",
            price: 300000,
            exh: 1,
            or: "v",
            fileName: "paint-3.jpg",
          },
          {
            id: 5,
            name: "Beach",
            params: "2020, Argalit, acril, 100x150cm",
            price: 300000,
            exh: 1,
            or: "h",
            fileName: "paint-4.jpg",
          },
          {
            id: 6,
            name: "Jump",
            params: "2020, Argalit, acril, 100x150cm",
            price: 300000,
            exh: 1,
            or: "h",
            fileName: "paint-5.jpg",
          },
          {
            id: 7,
            name: "Flowers",
            params: "2020, Argalit, acril, 100x150cm",
            price: 300000,
            exh: 1,
            or: "v",
            fileName: "paint-6.jpg",
          },
          {
            id: 8,
            name: "Flowers 2",
            params: "2020, Argalit, acril, 100x150cm",
            price: 300000,
            exh: 1,
            or: "v",
            fileName: "paint-7.jpg",
          },
          {
            id: 9,
            name: "Flowers 3",
            params: "2020, Argalit, acril, 100x150cm",
            price: 300000,
            exh: 1,
            or: "v",
            fileName: "paint-8.jpg",
          },
        ],
        exhibitions: [
          {
            id: 1,
            name: "The Observer's Affect, The 6th Moscow International Biennale for Young Art, CCA Winzavod, curator Arseniy Sergeev",
            year: 2021,
          },
          {
            id: 2,
            name: "New Slogans, hse art gallery, curator Alexander Shaburov",
            year: 2021,
          },
          {
            name: "Copied with a Lot of Reservations project, The Vyksa Artist-in-Residence, Vyksa",
            year: 2021,
          },
          {
            name: "Ray marching, volumes convincingly lit, material set to unlit, subsurface scattering, NPC under IBL, Triangle Gallery, Moscow, Russia",
            year: 2021,
          },
          {
            name: "The Speech of the Transparency Dictat, Triangle Gallery, Moscow, Russia",
            year: 2021,
          },
          {
            name: "Copied with a Lot of Reservations project, The Vyksa Artist-in-Residence, Vyksa",
            year: 2021,
          },
          {
            name: "The Speech of the Transparency Dictat, Triangle Gallery, Moscow, Russia",
            year: 2021,
          },
          {
            name: "Ray marching, volumes convincingly lit, material set to unlit, subsurface scattering, NPC under IBL, Triangle Gallery, Moscow, Russia",
            year: 2021,
          },
          {
            name: "The Speech of the Transparency Dictat, Triangle Gallery, Moscow, Russia",
            year: 2021,
          },
          {
            name: "Copied with a Lot of Reservations project, The Vyksa Artist-in-Residence, Vyksa",
            year: 2021,
          },
          {
            name: "Ray marching, volumes convincingly lit, material set to unlit, subsurface scattering, NPC under IBL, Triangle Gallery, Moscow, Russia",
            year: 2021,
          },
          {
            name: "Copied with a Lot of Reservations project, The Vyksa Artist-in-Residence, Vyksa",
            year: 2021,
          },
          {
            name: "The Speech of the Transparency Dictat, Triangle Gallery, Moscow, Russia",
            year: 2021,
          },
        ],
        interview: {
          cover: "artist-interview.jpg",
          videoCode: "",
        },
      },
      {
        id: 2,
        name: "Aleksey Vasiliev",
        url: "artist-2",
        folder: "paints-2",
        startPhoto: "Start-2.jpg",
        paints: [
          {
            id: 1,
            name: "Waves",
            params: "2020, Argalit, acril, 100x150cm",
            price: 300000,
            exh: 1,
            or: "h",
            fileName: "photo_2023-12-08_17-22-55.jpg",
          },
          {
            id: 2,
            name: "Kids",
            params: "2020, Argalit, acril, 100x150cm",
            price: 300000,
            exh: 1,
            or: "v",
            fileName: "photo_2023-12-08_17-23-00.jpg",
          },
          {
            id: 3,
            name: "Kids",
            params: "2020, Argalit, acril, 100x150cm",
            price: 300000,
            exh: 1,
            or: "h",
            fileName: "photo_2023-12-08_17-23-03.jpg",
          },
          {
            id: 4,
            name: "Kids",
            params: "2020, Argalit, acril, 100x150cm",
            price: 300000,
            exh: 1,
            or: "v",
            fileName: "photo_2023-12-08_17-23-07.jpg",
          },
          {
            id: 5,
            name: "Kids",
            params: "2020, Argalit, acril, 100x150cm",
            price: 300000,
            exh: 1,
            or: "v",
            fileName: "photo_2023-12-08_17-23-12.jpg",
          },
          {
            id: 6,
            name: "Kids",
            params: "2020, Argalit, acril, 100x150cm",
            price: 300000,
            exh: 1,
            or: "v",
            fileName: "photo_2023-12-08_17-23-16.jpg",
          },
        ],
        interview: {
          cover: "",
          videoCode: "",
        },
      },
      {
        id: 3,
        name: "MOMA",
        url: "artist-3",
        startPhoto: "Start.jpg",
        paints: [],
        interview: {
          cover: "",
          videoCode: "",
        },
      },
      {
        id: 4,
        name: "Sasha Svyatoy",
        url: "artist-4",
        startPhoto: "Start-2.jpg",
        paints: [],
        interview: {
          cover: "",
          video_code: "",
        },
      },
      {
        id: 5,
        name: "Margarita Varakina",
        url: "artist-5",
        startPhoto: "Start.jpg",
        paints: [],
        interview: {
          cover: "",
          videoCode: "",
        },
      },
      {
        id: 6,
        name: "Ivan Arkhipov",
        url: "artist-6",
        startPhoto: "Start-2.jpg",
        paints: [],
        interview: {
          cover: "",
          videoCode: "",
        },
      },
    ],
  },
  getters: {
    // params: (state, getters) => {
    //   return getters.params;
    // },
    getParams: (state) => () => {
      return state.params
    },
    getExhibitionById: (state) => (id) => {
      return state.exhibitions.find((item) => {
        return item.id == id
      })
    },
    getArtistById: (state) => (id) => {
      return state.artists.find((item) => {
        return item.id == id
      })
    },
    getArtistByUrl: (state) => (url) => {
      return state.artists.find((item) => {
        return item.url == url
      })
    },
    getCatalogue: (state) => (withShuffle) => {
      let _paints = []
      state.artists.forEach((artist) => {
        artist.paints.forEach((painting) => {
          painting.artist = artist.name
          painting.folder = artist.folder
          _paints.push(painting)
        })
        // _paints = _paints.concat(artist.paints)
      })
      if (withShuffle) {
        _paints = _paints
          .map((value) => ({ value, sort: Math.random() }))
          .sort((a, b) => a.sort - b.sort)
          .map(({ value }) => value)
        return _paints
      } else return _paints
    },
  },
  mutations: {},
  actions: {},
  modules: {},
})
